#!/bin/bash

# Show running SRT instances

declare -A srts
IFS=$'\n'       # make newlines the only separator
for p in $(ps -e -o pid,cmd | grep "manage.py runserver" | grep 'srt' ) ; do
    if [ "${p}" != "${p/grep/}" ] ; then
        continue
    fi
    d=${p#* }     # strip python path
    d=${d#* }     # strip python path
    p=${p# *}     # strip leading spaces
    pid=${p%% *}  # strip python path
    echo "[$pid]($d)"
done

