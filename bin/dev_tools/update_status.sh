#!/bin/sh

#
# Helper routine to see if any active update commands are executing
# in addition to showing the the backgroup updater is running.
#
# Sample result:
# $ ./is_update.sh
# 18149 python3 /opt/srt/bin/common/srtool_update.py --cron-start
# Update:2019-03-16 12:29:21,bin/common/srtool_common.py --score-new-cves NEW --count=100
# Done:2019-03-16 12:29:49,bin/common/srtool_common.py --score-new-cves NEW --count=100
#
# An "Update" without a "Done" is an running tack
#

# Test if the backgroup updater is running
if [ -f .srtupdate.pid ] ; then
	pid=`cat .srtupdate.pid`
	updater=`ps -e -o pid,cmd | grep $pid | grep -v grep | grep cron`
else
	echo "No updater pid file found"
	updater=""
fi
if [ -z "$updater" ] ; then
    echo "!!! WARNING: UPDATER IS NOT RUNNING !!!"
	cat .srtupdate.task
    exit 1
else
	echo "UPDATER:$updater"
fi

# Test if their is an open update in progress
cat .srtupdate.task
is_start=`grep "^Update" .srtupdate.task | grep -v "<cron_start>"` 
is_stop=`grep "^Done" .srtupdate.task` 
if [ -z "$is_stop" ] ; then
    echo "!!! UPDATE JOB RUNNING !!!"
    exit 1
else
    echo "UPDATE PAUSED BETWEEN JOBS."
    exit 0
fi

