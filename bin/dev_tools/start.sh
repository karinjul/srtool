#!/bin/bash

# SRTool helper script to start the instance

if [ -z "$SRT_PORT" ] ; then
    SRT_PORT=9000
fi

# Accept parameters (like 'noautoupdate')
./bin/srt start webport=0.0.0.0:$SRT_PORT $1

