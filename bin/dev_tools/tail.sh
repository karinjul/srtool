#!/bin/bash

# SRTool helper script to quickly dump the log files

CONTEXT=$1
if [ -n "$CONTEXT" ] ; then
	CONTEXT="-n $CONTEXT"
fi

SRTDBG_ERR_LOG="./srt_errors.txt"

if [ -z "$SRTDBG_LOG" ] ; then
    SRTDBG_LOG=/tmp/srt_dbg.log
fi

echo "--- srt_web.log --------------------"
tail srt_web.log $CONTEXT
echo "---  $SRTDBG_ERR_LOG --------------------"
tail $SRTDBG_ERR_LOG $CONTEXT
echo "--- $SRTDBG_LOG --------------------"
tail $SRTDBG_LOG $CONTEXT
echo "--- Update Task --------------------"
cat .srtupdate.task

