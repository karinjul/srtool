[ Red Hat ]

From: https://www.redhat.com/en/about/terms-use

Copyrights

"Red Hat either owns the intellectual property rights in the HTML, text,
images audio, video, software or other content that is made available on
this website, or has obtained the permission of the owner of the intellectual
property to make it available on this website. Red Hat strictly prohibits
the redistribution or copying of any part of this website or content on this
website without written permission from Red Hat. Red Hat authorizes you to
display on your computer, download and print pages from this website
provided: (a) the copyright notice appears on all such printouts, (b) the
information will not be altered, (c) the content is only used for personal,
educational and non-commercial use, and (d) you do not redistribute or copy
the information to any other media. Red Hat and its affiliates respect the
intellectual property of others. If you believe that your work has been
copied in a way that constitutes copyright infringement, please follow our
Notice and Procedure for Making Claims of Copyright Infringement."



