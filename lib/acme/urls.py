from django.conf.urls import include, url
from . import views, tables

urlpatterns = [
    url(r'^hello/$', views.acme_hello, name='acme_hello'),

    url(r'^product/(?P<product_pk>\d+)$', views.acme_product, name="acmeproduct"),
    url(r'^products/$',
        tables.AcmeProductsTable.as_view(template_name="acmeproducts-toastertable.html"),
        name='acmeproducts'),

    url(r'^defects/$',
        tables.AcmeDefectsTable.as_view(template_name="acmedefects-toastertable.html"),
        name='acmedefects'),

    url(r'^$', views.acme_hello, name='acme_default'),

    url(r'^report/(?P<page_name>\D+)$', views.report, name='report'),
]
