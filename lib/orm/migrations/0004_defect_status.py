# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orm', '0003_modified'),
    ]

    operations = [
        migrations.AddField(
            model_name='defect',
            name='srt_status',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='defect',
            name='srt_outcome',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='defect',
            name='srt_priority',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='defect',
            name='duplicate_of',
            field=models.CharField(max_length=50, blank=True, default=''),
        ),

    ]
