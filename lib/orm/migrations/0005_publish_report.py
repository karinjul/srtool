# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

class Migration(migrations.Migration):

    dependencies = [
        ('orm', '0004_defect_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='PublishSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cve', models.ForeignKey(default=None, to='orm.cve', null=True, on_delete=models.CASCADE,)),
                ('state', models.IntegerField(default=0)),
                ('reason', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='DefectHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('defect', models.ForeignKey(to='orm.defect', on_delete=models.CASCADE,)),
                ('comment', models.TextField(blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('author', models.TextField(blank=True)),
            ],
        ),

    ]
