# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

from orm.models import CpeFilter

class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SrtSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=63)),
                ('helptext', models.TextField(blank=True)),
                ('value', models.CharField(max_length=255)),
            ],
        ),

        migrations.CreateModel(
            name='HelpText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('area', models.IntegerField(choices=[(0, b'variable')])),
                ('key', models.CharField(max_length=100)),
                ('text', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='DataSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=20)),
                ('data', models.CharField(max_length=20)),
                ('source', models.CharField(max_length=20)),
                ('name', models.CharField(max_length=20)),
                ('description', models.TextField(blank=True)),
                ('attributes', models.TextField(blank=True)),
                ('cve_filter', models.CharField(max_length=20)),
                ('init', models.TextField(blank=True)),
                ('update', models.TextField(blank=True)),
                ('lookup', models.TextField(blank=True)),
                ('update_frequency', models.IntegerField(default=2)),
                ('loaded', models.BooleanField(default=False)),
                ('lastModifiedDate', models.CharField(max_length=50, blank=True)),
                ('update_time', models.CharField(max_length=50, blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='CweTable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('href', models.TextField(blank=True)),
                ('summary', models.TextField(blank=True)),
                ('description', models.TextField(blank=True)),
                ('vulnerable_count', models.IntegerField(default=0)),
                ('found', models.BooleanField(default=False)),
            ],
        ),

        migrations.CreateModel(
            name='Cve',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('name_sort', models.CharField(max_length=50)),

                ('priority', models.IntegerField(default=0)),
                ('status', models.IntegerField(default=0)),
                ('comments', models.TextField(blank=True)),
                ('comments_private', models.TextField(blank=True)),

                ('cve_data_type', models.CharField(max_length=100, blank=True)),
                ('cve_data_format', models.CharField(max_length=50, blank=True)),
                ('cve_data_version', models.CharField(max_length=50, blank=True)),

                ('public', models.BooleanField(default=True)),
                ('publish_state', models.IntegerField(default=0)),
                ('publish_date', models.CharField(max_length=50, blank=True)),

                ('description', models.TextField(blank=True)),
                ('publishedDate', models.CharField(max_length=50, blank=True)),
                ('lastModifiedDate', models.CharField(max_length=50, blank=True)),

                ('recommend', models.IntegerField(default=0)),
                ('recommend_list', models.TextField(blank=True)),

                ('cvssV3_baseScore', models.CharField(max_length=50, blank=True)),
                ('cvssV3_baseSeverity', models.CharField(max_length=50, blank=True)),

                ('cvssV2_baseScore',models.CharField(max_length=50, blank=True)),
                ('cvssV2_severity', models.CharField(max_length=50, blank=True)),

                ('packages', models.TextField(blank=True)),

                ('score_date', models.DateTimeField(null=True, blank=True)),
                ('srt_updated', models.DateTimeField(auto_now=True)),
##                ('srt_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),

        migrations.CreateModel(
            name='CveLocal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),

                ('cve_data_type', models.CharField(max_length=100, blank=True)),
                ('cve_data_format', models.CharField(max_length=50, blank=True)),
                ('cve_data_version', models.CharField(max_length=50, blank=True)),

                ('description', models.TextField(blank=True)),
                ('publishedDate', models.CharField(max_length=50, blank=True)),
                ('lastModifiedDate', models.CharField(max_length=50, blank=True)),
                ('url', models.TextField(blank=True)),
                ('url_title', models.TextField(default='Link')),

                ('recommend', models.IntegerField(default=0)),
                ('recommend_list', models.TextField(blank=True)),

                ('cpe_list', models.TextField(blank=True)),

                ('cvssV3_baseScore', models.CharField(max_length=50, blank=True)),
                ('cvssV3_baseSeverity', models.CharField(max_length=50, blank=True)),
                ('cvssV3_vectorString', models.TextField(blank=True)),
                ('cvssV3_exploitabilityScore', models.CharField(max_length=50, blank=True)),
                ('cvssV3_impactScore', models.CharField(max_length=50, blank=True)),
                ('cvssV3_attackVector', models.CharField(max_length=5, blank=True)),
                ('cvssV3_attackComplexity', models.CharField(max_length=50, blank=True)),
                ('cvssV3_privilegesRequired', models.CharField(max_length=50, blank=True)),
                ('cvssV3_userInteraction', models.CharField(max_length=50, blank=True)),
                ('cvssV3_scope', models.CharField(max_length=50, blank=True)),
                ('cvssV3_confidentialityImpact', models.CharField(max_length=50, blank=True)),
                ('cvssV3_integrityImpact', models.CharField(max_length=50, blank=True)),
                ('cvssV3_availabilityImpact', models.CharField(max_length=50, blank=True)),

                ('cvssV2_baseScore',models.CharField(max_length=50, blank=True)),
                ('cvssV2_severity', models.CharField(max_length=50, blank=True)),
                ('cvssV2_vectorString', models.TextField(blank=True)),
                ('cvssV2_exploitabilityScore', models.CharField(max_length=50, blank=True)),
                ('cvssV2_impactScore', models.CharField(max_length=50, blank=True)),
                ('cvssV2_accessVector', models.CharField(max_length=50, blank=True)),
                ('cvssV2_accessComplexity', models.CharField(max_length=50, blank=True)),
                ('cvssV2_authentication', models.CharField(max_length=50, blank=True)),
                ('cvssV2_confidentialityImpact', models.CharField(max_length=50, blank=True)),
                ('cvssV2_integrityImpact', models.CharField(max_length=50, blank=True)),
            ],
        ),


        migrations.CreateModel(
            name='CveSource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cve', models.ForeignKey(default=None, related_name='source2cve', to='orm.cve', null=True,on_delete=models.CASCADE,)),
                ('datasource', models.ForeignKey(default=None, to='orm.datasource',null=True,on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='CveToCwe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cve', models.ForeignKey(related_name='cve2cwe', to='orm.cve', on_delete=models.CASCADE,)),
                ('cwe', models.ForeignKey(to='orm.cwetable', on_delete=models.CASCADE,)),
            ],
        ),


        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mode', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=50, blank=True)),
                ('realname', models.CharField(max_length=50, blank=True)),
                ('invalidname', models.TextField(blank=True)),
                ('weight', models.IntegerField(default=0)),
                ('cve_count', models.IntegerField(default=0)),
                ('vulnerability_count', models.IntegerField(default=0)),
                ('investigation_count', models.IntegerField(default=0)),
                ('defect_count', models.IntegerField(default=0)),
            ],
        ),

        migrations.CreateModel(
            name='PackageToCve',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('package', models.ForeignKey(related_name='package2cve', to='orm.package', on_delete=models.CASCADE,)),
                ('cve', models.ForeignKey(related_name='cve2package', to='orm.cve', on_delete=models.CASCADE,)),
                ('applicable', models.NullBooleanField(default=True, null=True)),
            ],
        ),

        migrations.CreateModel(
            name='CveReference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cve', models.ForeignKey(related_name='references', to='orm.cve', on_delete=models.CASCADE,)),
                ('hyperlink', models.CharField(max_length=100, null=True)),
                ('resource', models.CharField(max_length=100, null=True)),
                ('type', models.CharField(max_length=100, null=True)),
                ('source', models.CharField(max_length=100, null=True)),
                ('name', models.CharField(max_length=100, null=True)),
                ('datasource', models.ForeignKey(related_name='source_references', to='orm.datasource', default=None, null=True,on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='CveHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cve', models.ForeignKey(default=None, null=True, to='orm.cve', on_delete=models.CASCADE,)),
                ('comment', models.TextField(blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('author', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=0)),
                ('key', models.CharField(max_length=40)),
                ('name', models.CharField(max_length=40)),
                ('version', models.CharField(max_length=40)),
                ('profile', models.CharField(max_length=40)),
                ('cpe', models.CharField(max_length=40)),
                ('defect_tags', models.TextField(blank=True, default='')),
                ('product_tags', models.TextField(blank=True, default='')),
            ],
        ),

        migrations.CreateModel(
            name='Vulnerability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True, default='')),
                ('cve_primary_name', models.CharField(max_length=50, default='')),
                ('public', models.BooleanField(default=False)),
                ('comments', models.TextField(blank=True, default='')),
                ('comments_private', models.TextField(blank=True, default='')),
                ('status', models.IntegerField(default=0)),
                ('outcome', models.IntegerField(default=0)),
                ('priority', models.IntegerField(default=0)),
            ],
        ),

        migrations.CreateModel(
            name='CveToVulnerablility',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vulnerability', models.ForeignKey(to='orm.vulnerability', on_delete=models.CASCADE,)),
                ('cve', models.ForeignKey(to='orm.cve', on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='VulnerabilityComments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vulnerability', models.ForeignKey(to='orm.vulnerability', on_delete=models.CASCADE,)),
                ('comment', models.TextField(blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('author', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='VulnerabilityHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vulnerability', models.ForeignKey(to='orm.vulnerability', on_delete=models.CASCADE,)),
                ('comment', models.TextField(blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('author', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='VulnerabilityUploads',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vulnerability', models.ForeignKey(to='orm.vulnerability',on_delete=models.CASCADE,)),
                ('description', models.TextField(blank=True)),
                ('path', models.TextField(blank=True)),
                ('size', models.IntegerField(default=0)),
                ('date', models.DateField(null=True, blank=True)),
                ('author', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='Defect',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('summary', models.TextField(blank=True)),
                ('url', models.TextField(blank=True)),
                ('priority', models.IntegerField(default=0)),
                ('status', models.IntegerField(default=0)),
                ('resolution', models.IntegerField(default=0)),
                ('publish', models.TextField(blank=True)),
                ('release_version', models.CharField(max_length=50)),
                ('product', models.ForeignKey(to='orm.product', on_delete=models.CASCADE,)),
                ('date_created', models.CharField(max_length=50)),
                ('date_updated', models.CharField(max_length=50)),
                ('srt_updated', models.DateTimeField(auto_now=True)),
            ],
        ),


        migrations.CreateModel(
            name='Investigation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('vulnerability', models.ForeignKey(related_name='vulnerability_investigation',to='orm.vulnerability', on_delete=models.CASCADE,)),
                ('product', models.ForeignKey(related_name='references', to='orm.product',on_delete=models.CASCADE,)),

                ('public', models.BooleanField(default=True)),
                ('comments', models.TextField(blank=True)),
                ('comments_private', models.TextField(blank=True)),

                ('status', models.IntegerField(default=0)),
                ('outcome', models.IntegerField(default=0)),
                ('priority', models.IntegerField(default=0)),
            ],
        ),

        migrations.CreateModel(
            name='InvestigationToDefect',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('investigation', models.ForeignKey(to='orm.investigation', on_delete=models.CASCADE,)),
                ('defect', models.ForeignKey(to='orm.defect', on_delete=models.CASCADE,)),
                ('product', models.ForeignKey(to='orm.product', on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='InvestigationComments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('investigation', models.ForeignKey(to='orm.investigation', on_delete=models.CASCADE,)),
                ('comment', models.TextField(blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('author', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='InvestigationHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('investigation', models.ForeignKey(to='orm.investigation', on_delete=models.CASCADE,)),
                ('comment', models.TextField(blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('author', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='InvestigationUploads',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('investigation', models.ForeignKey(to='orm.investigation', on_delete=models.CASCADE,)),
                ('description', models.TextField(blank=True)),
                ('path', models.TextField(blank=True)),
                ('size', models.IntegerField(default=0)),
                ('date', models.DateField(null=True, blank=True)),
                ('author', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='VulnerabilityToInvestigation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vulnerability', models.ForeignKey(related_name='vulnerability_investigation', to='orm.vulnerability',on_delete=models.CASCADE,)),
                ('investigation', models.ForeignKey(related_name='investigation_vulnerability', to='orm.investigation',on_delete=models.CASCADE,)),
            ],
        ),


        migrations.CreateModel(
            name='VulnerabilityAccess',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vulnerability', models.ForeignKey(to='orm.vulnerability', on_delete=models.CASCADE,)),
                ('user', models.ForeignKey(to='users.srtuser', on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='InvestigationAccess',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('investigation', models.ForeignKey(to='orm.investigation', on_delete=models.CASCADE,)),
                ('user', models.ForeignKey(to='users.srtuser', on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='VulnerabilityNotification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vulnerability', models.ForeignKey(to='orm.vulnerability', on_delete=models.CASCADE,)),
                ('user', models.ForeignKey(to='users.srtuser', on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='InvestigationNotification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('investigation', models.ForeignKey(to='orm.investigation', on_delete=models.CASCADE,)),
                ('user', models.ForeignKey(to='users.srtuser', on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='CpeTable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vulnerable', models.BooleanField(default='False')),
                ('cpeMatchString', models.TextField(blank=True)),
                ('cpe23Uri', models.TextField(blank=True)),
                ('versionEndIncluding', models.TextField(blank=True)),
            ],
        ),

        migrations.CreateModel(
            name='CpeToCve',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cpe', models.ForeignKey(to='orm.cpetable',on_delete=models.CASCADE,)),
                ('cve', models.ForeignKey(to='orm.cve',on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='CpeFilter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key_prime', models.CharField(max_length=50)),
                ('key_sub', models.CharField(max_length=50)),
                ('status', models.IntegerField(default=CpeFilter.UNDECIDED)),
                ('automatic', models.BooleanField(default=False)),
            ],
#            unique_together=set([('key_prime', 'key_sub')]),
        ),


        migrations.CreateModel(
            name='PublishPending',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cve', models.ForeignKey(default=None, to='orm.cve',blank=True,null=True,on_delete=models.CASCADE,)),
                ('vulnerability', models.ForeignKey(default=None, to='orm.vulnerability',blank=True,null=True,on_delete=models.CASCADE,)),
                ('investigation', models.ForeignKey(default=None, to='orm.investigation',blank=True,null=True,on_delete=models.CASCADE,)),
                ('date', models.DateField(null=True, blank=True)),
                ('note', models.TextField()),
            ],
        ),


        migrations.CreateModel(
            name='Notify',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(max_length=50)),
                ('description', models.TextField()),
                ('url', models.TextField()),
                ('priority', models.IntegerField(default=0)),
                ('author', models.TextField()),
##                ('srt_updated', models.DateTimeField(auto_now=True)),
##                ('srt_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),

        migrations.CreateModel(
            name='NotifyAccess',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notify', models.ForeignKey(default=None, to='orm.notify',blank=True,null=True,on_delete=models.CASCADE,)),
                ('user', models.ForeignKey(default=None, to='users.srtuser',blank=True,null=True,on_delete=models.CASCADE,)),
            ],
        ),

        migrations.CreateModel(
            name='NotifyCategories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(max_length=50)),
            ],
        ),

    ]
