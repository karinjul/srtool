# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orm', '0002_updates'),
    ]

    operations = [
        migrations.AddField(
            model_name='cve',
            name='srt_created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AddField(
            model_name='cve',
            name='tags',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AddField(
            model_name='cve',
            name='acknowledge_date',
            field=models.DateTimeField(null=True),
        ),

        migrations.AddField(
            model_name='investigation',
            name='tags',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AddField(
            model_name='investigation',
            name='srt_created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AddField(
            model_name='investigation',
            name='srt_updated',
            field=models.DateTimeField(auto_now=True),
        ),

        migrations.AddField(
            model_name='vulnerability',
            name='tags',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AddField(
            model_name='vulnerability',
            name='srt_created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AddField(
            model_name='vulnerability',
            name='srt_updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
