from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^hello/$', views.yp_hello, name='yp_hello'),

    url(r'^$', views.yp_hello, name='yp_default'),

    url(r'^report/(?P<page_name>\D+)$', views.report, name='report'),
    url(r'^manage_report/$', views.manage_report, name='manage_report'),

]
