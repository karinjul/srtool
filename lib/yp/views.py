#
# ex:ts=4:sw=4:sts=4:et
# -*- tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# Security Response Tool Implementation
#
# Copyright (C) 2017-2020 Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#from django.urls import reverse_lazy
#from django.views import generic
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

from users.models import SrtUser, UserSafe
from srtgui.views import MimeTypeFinder
from yp.reports import YPReportManager

#from orm.models import SrtSetting

# quick development/debugging support
from srtgui.api import _log

def yp_hello(request):
    context = {}
    _log("Note:yp_hello")
    return render(request, 'yp_hello.html', context)

def report(request,page_name):
    if request.method == "GET":
        context = YPReportManager.get_context_data(page_name,request=request)
        record_list = request.GET.get('record_list', '')
        _log("EXPORT_GET!:%s|%s|" % (request,record_list))
        context['record_list'] = record_list
        return render(request, 'report.html', context)
    elif request.method == "POST":
        _log("EXPORT_POST!:%s|%s" % (request,request.FILES))
        parent_page = request.POST.get('parent_page', '')
        file_name,response_file_name = YPReportManager.exec_report(parent_page,request=request)

        if file_name.startswith("Error"):
            # Refresh the page with the error message
            context = YPReportManager.get_context_data(page_name,request=request)
            context['error_message'] = file_name
            record_list = request.GET.get('record_list', '')
            _log("EXPORT_GET_WITH_ERROR!:%s|%s|" % (request,record_list))
            context['record_list'] = record_list
            return render(request, 'report.html', context)
        elif file_name and response_file_name:
            fsock = open(file_name, "rb")
            content_type = MimeTypeFinder.get_mimetype(file_name)

            response = HttpResponse(fsock, content_type = content_type)

            disposition = "attachment; filename=" + response_file_name
            response["Content-Disposition"] = disposition

            _log("EXPORT_POST_Q{%s|" % (response))
            return response
        else:
            return render(request, "unavailable_artifact.html", {})

        return redirect('/')
    raise Exception("Invalid HTTP method for this page")

def manage_report(request):
    # does this user have permission to see this record?
    if not UserSafe.is_creator(request.user):
        return redirect('/')

    return redirect(report,'management')
