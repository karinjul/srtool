from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from django.db import OperationalError
import os

class Command(BaseCommand):
    help = "Trigger a data source update"

    def add_arguments(self, parser):
        print("UPDATE:add_arguments")
        parser.add_argument('--cron-start', action='store_const', const='cron_start', dest='command', help='Start the SRTool backgroud updater')
        parser.add_argument('--cron-stop', action='store_const', const='cron_stop', dest='command', help='Stop the SRTool backgroud updater')

        parser.add_argument('--list', '-l', action='store_const', const='list', dest='command', help='List data sources')
        parser.add_argument('--run-updates', '-u', action='store_const', const='run-updates', dest='command', help='update scheduled data sources')
        parser.add_argument('--force', '-f', action='store_true', dest='force', help='Force the update')
        parser.add_argument('--update-skip-history', '-H', action='store_true', dest='update_skip_history', help='Skip history updates for cummulative status')

        parser.add_argument('--verbose', action='store_true', dest='verbose', help='Debugging: verbose output')
        parser.add_argument('--trial', '-t', action='store_true', dest='is_trial', help='Debugging: trial run')

        # NOTE: we have to do shenanigans with name_filter to support spaces
        parser.add_argument('--name-filter', '-n', nargs='+', dest='name_filter', help='Filter for datasource name')

    def handle(self, *args, **options):
        print("UPDATE:%s|%s" % (str(args),str(options)))

        command = ''
        if 'cron_start' == options['command']: command = '--cron-start'
        if 'cron_stop' == options['command']: command = '--cron-stop'
        if 'list' == options['command']: command = '--list'
        if 'run-updates' == options['command']: command = '--run-updates'
        if not command:
            print("manage update: missing command '%s %s'" % (str(args),str(options)))
        else:
            if options['verbose'] or (options['verbosity'] > 1):
                command += ' --verbose'
                verbose = True
            else:
                verbose = False
            if options['force']: command += ' --force'
            if options['update_skip_history']: command += ' --update-skip-history'
            if options['is_trial']: command += ' --trial'
            # NOTE: we have to do shenanigans with name_filter to support spaces
            if options['name_filter']: command += ' --name-filter "%s"' % ' '.join(options['name_filter'])
            if 'cron_start' == options['command']: command += ' > /dev/null 2>&1 &'
            update_command = "./bin/common/srtool_update.py %s" % (command)
            if verbose:
                print("RUN UPDATE SCRIPT: %s" % (update_command))
            os.chdir(os.environ['SRT_BASE_DIR'])
            os.system("%s" % (update_command))
