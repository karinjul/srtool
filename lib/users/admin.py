from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import SrtUserCreationForm, SrtUserChangeForm
from .models import SrtUser

class SrtUserAdmin(UserAdmin):
    add_form = SrtUserCreationForm
    form = SrtUserChangeForm
    model = SrtUser
    list_display = ['email', 'username', 'role']

admin.site.register(SrtUser, SrtUserAdmin)
